package com.impaq.spring.atm.transport;

/**
 * Author: Adam Pierzchała
 * Date: 06/05/2013
 */
public interface ATMTransport {
    void communicateWithBank(byte[] message);
}
