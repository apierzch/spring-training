package com.impaq.spring;


import com.impaq.spring.atm.AutomatedTellerMachine;
import com.impaq.spring.atm.AutomatedTellerMachineImpl;
import com.impaq.spring.atm.auth.JdbcAuthenticationService;
import com.impaq.spring.atm.auth.UnauthorizedException;
import com.impaq.spring.atm.transport.ATMTransport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Author: Adam Pierzchała
 * Date: 06/05/2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:application-context.xml"})
@Transactional
public class ATMOperationsTest {

    private final static BigDecimal testAmount = new BigDecimal(100);
    private final ATMTransport transport = mock(ATMTransport.class);
    // TODO 1: can we use interface here? (spring beans!)
    private AutomatedTellerMachineImpl atm = new AutomatedTellerMachineImpl();

    @Before
    public void setup() {
        atm.setTransport(transport);
    }

    @Test
    public void shouldSendPositiveToBank() {
        // when
        authenticate(atm);
        atm.deposit(testAmount);

        // then
        verify(transport).communicateWithBank(bytesEqualBigDecimal(testAmount));
    }

    private void authenticate(AutomatedTellerMachineImpl atm) {
        // TODO 1: use spring bean instead of setter
        atm.setAuthenticationService(new JdbcAuthenticationService());
        atm.authenticate(1234);
    }

    @Test
    public void shouldSendNegativeToBank() {
        // when
        authenticate(atm);
        atm.withdraw(testAmount);

        // then
        verify(transport).communicateWithBank(bytesEqualBigDecimal(testAmount.multiply(AutomatedTellerMachine.NEG_ONE)));
    }

    private byte[] bytesEqualBigDecimal(BigDecimal money) {
        return argThat(new ByteArrayEqualsBigDecimal(money));
    }
}
