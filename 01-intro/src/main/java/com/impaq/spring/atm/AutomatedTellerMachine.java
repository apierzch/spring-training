package com.impaq.spring.atm;

import java.math.BigDecimal;

/**
 * Author: Adam Pierzchała
 * Date: 06/05/2013
 */
public interface AutomatedTellerMachine {
    BigDecimal NEG_ONE = new BigDecimal(-1);

    void deposit(BigDecimal money);

    void withdraw(BigDecimal money);

    void authenticate(Integer pinNumber);
}
