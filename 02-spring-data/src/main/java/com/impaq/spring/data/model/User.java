package com.impaq.spring.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Author: Adam Pierzchała
 * Date: 06/05/2013
 */
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long objectId;

    private String firstname;
    private String lastname;

    private Integer age;
}
