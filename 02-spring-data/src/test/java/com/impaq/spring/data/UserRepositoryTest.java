package com.impaq.spring.data;

import com.impaq.spring.data.config.ApplicationConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * Author: Adam Pierzchała
 * Date: 06/05/2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class, loader = AnnotationConfigContextLoader.class)
public class UserRepositoryTest {

    // TODO: implement tests

    @Autowired
    private UserRepository repository;

    @Test
    public void shouldFindOneUser() {
        String firstname = "Bob";

        // find by firstname
    }

    @Test
    public void shouldFindNoUsers() {
        String firstname = "Alex";

        // find by firstname
    }

    @Test
    public void shouldFindOneUserWithComplexQuery() {
        String firstname = "Bob";
        String lastname = "Budowniczy";
        Integer minimumAge = 24;

        // find by firstname + lastname, but older than minimumAge
    }
}
