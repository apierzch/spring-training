package com.impaq.spring.atm.transport;

/**
 * Author: Adam Pierzchała
 * Date: 06/05/2013
 */
public class SoapATMTransport implements ATMTransport {
    @Override
    public void communicateWithBank(byte[] message) {
        System.out.println("communication using soap: " + new String(message));
    }
}
