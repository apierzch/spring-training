package com.impaq.spring;

import org.mockito.ArgumentMatcher;

import java.math.BigDecimal;

/**
 * Author: Adam Pierzchała
 * Date: 06/05/2013
 */
public class ByteArrayEqualsBigDecimal extends ArgumentMatcher<byte[]> {
    protected BigDecimal amount;

    public ByteArrayEqualsBigDecimal(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean matches(Object argument) {
        byte[] arg = (byte[]) argument;
        return new String(arg).equals(amount.toString());
    }
}
