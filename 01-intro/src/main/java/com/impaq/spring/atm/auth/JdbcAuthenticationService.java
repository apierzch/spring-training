package com.impaq.spring.atm.auth;

import com.impaq.spring.atm.CreditCard;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * Author: Adam Pierzchała
 * Date: 06/05/2013
 */
public class JdbcAuthenticationService implements ATMAuthenticationService {
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean authenticate(CreditCard card, Integer pinCode) {
        // TODO 3: implement using JDBC
        return true;
    }

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

}
