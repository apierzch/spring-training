package com.impaq.spring;

import com.impaq.spring.atm.AutomatedTellerMachineImpl;
import com.impaq.spring.atm.auth.UnauthorizedException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.junit.Assert.fail;

/**
 * Author: Adam Pierzchała
 * Date: 06/05/2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:application-context.xml"})
@Transactional
public class ATMAuthorizationTest {
    private final static BigDecimal testAmount = new BigDecimal(100);

    // TODO 1: can we use interface here? (spring beans!)
    private AutomatedTellerMachineImpl atm = new AutomatedTellerMachineImpl();

    @Test
    public void shouldNotAllowUnauthorizedAccess() {
        try { // when
            atm.withdraw(testAmount);
            fail();
        } catch (UnauthorizedException e) { // then
            // OK
        }
    }
}
