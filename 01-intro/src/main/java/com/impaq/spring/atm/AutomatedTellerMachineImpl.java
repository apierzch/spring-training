package com.impaq.spring.atm;

import com.impaq.spring.atm.auth.ATMAuthenticationService;
import com.impaq.spring.atm.auth.UnauthorizedException;
import com.impaq.spring.atm.transport.ATMTransport;

import java.math.BigDecimal;

/**
 * Author: Adam Pierzchała
 * Date: 06/05/2013
 */
public class AutomatedTellerMachineImpl implements AutomatedTellerMachine {

    private ATMTransport transport;
    private ATMAuthenticationService authenticationService;
    private boolean authorized = false;

    @Override
    public void deposit(BigDecimal money) {
        // TODO 2: check authorization, check account validity and permissions/roles, both on deposit and withdraw...
        if (authorized) {
            transport.communicateWithBank(money.toString().getBytes());
        } else {
            throw new UnauthorizedException();
        }
    }

    @Override
    public void withdraw(BigDecimal money) {
        // TODO 2: check authorization, check account validity and permissions/roles, both on deposit and withdraw...
        if (authorized) {
            transport.communicateWithBank(money.multiply(NEG_ONE).toString().getBytes());
        } else {
            throw new UnauthorizedException();
        }
    }

    @Override
    public void authenticate(Integer pinNumber) {
        if (!authenticationService.authenticate(getInsertedCard(), pinNumber)) {
            throw new UnauthorizedException();
        }
        authorized = true;
    }

    private CreditCard getInsertedCard() {
        return null;
    }

    public void setTransport(ATMTransport transport) {
        this.transport = transport;
    }

    public void setAuthenticationService(ATMAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }
}
