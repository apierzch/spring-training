package com.impaq.spring.atm.auth;

import com.impaq.spring.atm.CreditCard;

/**
 * Author: Adam Pierzchała
 * Date: 06/05/2013
 */
public interface ATMAuthenticationService {
    boolean authenticate(CreditCard card, Integer pinCode);
}
